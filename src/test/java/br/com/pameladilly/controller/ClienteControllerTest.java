package br.com.pameladilly.controller;

import br.com.pameladilly.domain.entity.Cliente;
import br.com.pameladilly.exception.cliente.ClienteCPFDuplicadoException;
import br.com.pameladilly.exception.cliente.ClienteNotFoundException;
import br.com.pameladilly.rest.controller.ClienteController;
import br.com.pameladilly.rest.dto.ClienteRequestDTO;
import br.com.pameladilly.service.ClienteService;
import br.com.pameladilly.service.ClienteServiceTest;
import org.json.JSONObject;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@ExtendWith(SpringExtension.class)
@ActiveProfiles(value = "test")
@WebMvcTest(controllers = ClienteController.class)
@AutoConfigureMockMvc
public class ClienteControllerTest {

    static private final String CLIENTE_API = "/api/cliente";

    @Autowired
    MockMvc mockMvc;

    @MockBean
    ClienteService service;

    @Test
    @DisplayName("POST - Deve salvar um cliente")
    public void salvarClienteTest() throws Exception{
        ClienteRequestDTO clienteRequestDTO = ClienteServiceTest.getNewClienteDTO();

        Cliente cliente = ClienteServiceTest.getNewCliente();
        cliente.setId(1L);

        BDDMockito.given( service.salvar(Mockito.any(ClienteRequestDTO.class))).willReturn(cliente);

        JSONObject json =  new JSONObject();
        json.put("nome", clienteRequestDTO.getNome());
        json.put("nascimento", "04/09/1989");
        json.put("cpf", "58850960077");


        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .post(CLIENTE_API)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(json.toString());

        mockMvc.perform( request )
                .andExpect(MockMvcResultMatchers.status().isCreated());

    }

    @Test
    @DisplayName("POST - Deve retornar bad request - campos não informados")
    public void salvarClienteBadRequestCamposTest() throws Exception{
        ClienteRequestDTO clienteRequestDTO = ClienteServiceTest.getNewClienteDTO();

        JSONObject json =  new JSONObject();
        json.put("nome", clienteRequestDTO.getNome());


        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .post(CLIENTE_API)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(json.toString());

        mockMvc.perform( request )
                .andExpect(MockMvcResultMatchers.status().isBadRequest());

        BDDMockito.verify( service, BDDMockito.never()).salvar(Mockito.any(ClienteRequestDTO.class));
    }

    @Test
    @DisplayName("POST - Deve retornar bad request - CPF duplicado")
    public void salvarClienteBadRequestCpfDuplicado() throws Exception{

        ClienteRequestDTO clienteRequestDTO = ClienteServiceTest.getNewClienteDTO();

        Cliente cliente = ClienteServiceTest.getNewCliente();
        cliente.setId(1L);

        BDDMockito.given( service.salvar(Mockito.any(ClienteRequestDTO.class))).willThrow(new ClienteCPFDuplicadoException());

        JSONObject json =  new JSONObject();
        json.put("nome", clienteRequestDTO.getNome());
        json.put("nascimento", "04/09/1989");
        json.put("cpf", "58850960077");


        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .post(CLIENTE_API)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(json.toString());

        mockMvc.perform( request )
                .andExpect(MockMvcResultMatchers.status().isBadRequest());

    }

    @Test
    @DisplayName("PUT - Deve atualizar um cliente")
    public void atualizarClienteTest() throws Exception{

        ClienteRequestDTO clienteRequestDTO = ClienteServiceTest.getNewClienteDTO();

        Cliente clienteMock = ClienteServiceTest.getNewCliente();
        clienteMock.setId(1L);

        String nomeAtualizado = "Marcelo Gonçalvez";
        clienteMock.setNome(nomeAtualizado);

        BDDMockito.given( service.atualizar(Mockito.anyLong(), Mockito.any(ClienteRequestDTO.class))).willReturn(clienteMock);

        JSONObject json =  new JSONObject();
        json.put("nome", nomeAtualizado);
        json.put("nascimento", "04/09/1989");
        json.put("cpf", "58850960077");

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.put(CLIENTE_API.concat("/" + 1L))
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json.toString());

        mockMvc.perform( request )
                .andExpect( MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("nome").value(nomeAtualizado));


    }


    @Test
    @DisplayName("DELETE - Deve deletar um cliente")
    public void deletarClienteTest() throws Exception{


        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.delete(CLIENTE_API.concat("/" + 1L));

        mockMvc.perform( request )
                .andExpect( MockMvcResultMatchers.status().isOk());

    }

    @Test
    @DisplayName("DELETE - Deve retornar not found por cliente inexistente")
    public void deletarClienteInexistenteTest() throws Exception{

        BDDMockito.doThrow( ClienteNotFoundException.class).when( service ).excluir(Mockito.anyLong());

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.delete(CLIENTE_API.concat("/" + 1L));

        mockMvc.perform( request )
                .andExpect( MockMvcResultMatchers.status().isNotFound());
    }







}
