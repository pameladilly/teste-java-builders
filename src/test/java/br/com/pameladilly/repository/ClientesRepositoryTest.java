package br.com.pameladilly.repository;

import br.com.pameladilly.domain.entity.Cliente;
import br.com.pameladilly.domain.repository.ClientesRepository;
import br.com.pameladilly.service.ClienteServiceTest;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@ActiveProfiles(value = "test")
@DataJpaTest
public class ClientesRepositoryTest {

    @Autowired
    TestEntityManager entityManager;

    @Autowired
    ClientesRepository repository;

    @Test
    @DisplayName("Deve salvar um cliente")
    public void salvarCliente(){

        Cliente cliente = ClienteServiceTest.getNewCliente();

        Cliente clienteSalvo = entityManager.persist(cliente);

        Assertions.assertThat( clienteSalvo).isNotNull() ;
    }

    @Test
    @DisplayName("Atualizar cliente")
    public void atualizarCliente(){
        Cliente clienteSalvo = criarEPersistirCliente();

        Cliente cliente = ClienteServiceTest.getNewCliente();
        cliente.setId(clienteSalvo.getId());

        String nomeAtualizado = "Pamela Poletto";

        cliente.setNome(nomeAtualizado);

        repository.save(cliente);

        Cliente clienteAtualizado = entityManager.find(Cliente.class, cliente.getId());

        Assertions.assertThat(clienteAtualizado.getNome()).isEqualTo(nomeAtualizado);
    }

    @Test
    @DisplayName("Excluir cliente")
    public void excluirCliente(){

        Cliente cliente = criarEPersistirCliente();

        cliente = entityManager.find(Cliente.class, cliente.getId());

        repository.delete(cliente);

        Cliente clienteInexistente = entityManager.find(Cliente.class, cliente.getId());

        Assertions.assertThat(clienteInexistente).isNull();

    }

    @Test
    @DisplayName("Buscar cliente por CPF")
    public void existsByCpf(){

        Cliente cliente = criarEPersistirCliente();
        String cpf = cliente.getCpf();

        boolean existeClienteCpf = repository.existsByCpf(cpf);

        Assertions.assertThat(existeClienteCpf).isTrue();
    }

    @Test
    @DisplayName("Buscar cliente por CPF ou Nome")
    public void encontrarClienteCpfOuNome(){

        Cliente cliente = criarEPersistirCliente();

        Page<Cliente> result = repository.findByNomeOrCpf("Fulano", "", PageRequest.of(0, 10));

        Assertions.assertThat(result.getContent()).hasSize(1);
        Assertions.assertThat(result.getContent()).contains(cliente);
        Assertions.assertThat(result.getPageable().getPageSize()).isEqualTo(10);
        Assertions.assertThat(result.getPageable().getPageNumber()).isEqualTo(0);
        Assertions.assertThat(result.getTotalElements()).isEqualTo(1L);

    }

    private Cliente criarEPersistirCliente(){
        Cliente cliente = ClienteServiceTest.getNewCliente();
        return  entityManager.persist(cliente);

    }
}
