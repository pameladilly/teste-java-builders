package br.com.pameladilly.service;

import br.com.pameladilly.domain.entity.Cliente;
import br.com.pameladilly.domain.repository.ClientesRepository;
import br.com.pameladilly.exception.cliente.ClienteCPFDuplicadoException;
import br.com.pameladilly.exception.cliente.ClienteNotFoundException;
import br.com.pameladilly.rest.dto.ClienteRequestDTO;
import br.com.pameladilly.service.impl.ClienteServiceImpl;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.LocalDate;
import java.util.Optional;

@ExtendWith(value = SpringExtension.class)
@ActiveProfiles(value = "test")
public class ClienteServiceTest {

    ClienteService service;

    @MockBean
    ClientesRepository repository;

    @BeforeEach
    public void setUp(){
        this.service = new ClienteServiceImpl(repository);
    }

    @Test
    @DisplayName("Deve salvar um cliente")
    public void salvarClienteTest(){
        Cliente cliente = getNewCliente();
        cliente.setId(1L);

        Mockito.when( repository.save(Mockito.any(Cliente.class))).thenReturn( cliente );

        Cliente clienteSalvo = service.salvar(getNewClienteDTO());

        Assertions.assertThat(clienteSalvo).isNotNull();
        Assertions.assertThat(clienteSalvo.getId()).isNotNull();


    }


    @Test
    @DisplayName("Deve lançar exceção por cliente com CPF duplicado")
    public void clienteComCPFDuplicado(){

        ClienteRequestDTO clienteRequestDTO = getNewClienteDTO();

        Mockito.when( repository.existsByCpf(clienteRequestDTO.getCpf())).thenReturn(true);

        Throwable exception = Assertions.catchThrowable(() -> service.salvar(clienteRequestDTO));


        Assertions.assertThat(exception).isInstanceOf(ClienteCPFDuplicadoException.class);

        Mockito.verify( repository, Mockito.never()).save(Mockito.any(Cliente.class));
    }

    @Test
    @DisplayName("Deve atualizar um cliente")
    public void atualizarCliente(){
        ClienteRequestDTO clienteRequestDTO = getNewClienteDTO();
        Cliente cliente = getNewCliente();
        cliente.setId(1L);

        Mockito.when( repository.existsByCpf(Mockito.anyString())).thenReturn(false);
        Mockito.when( repository.save(Mockito.any(Cliente.class))).thenReturn(cliente);
        Mockito.when( repository.findById(Mockito.anyLong())).thenReturn(Optional.of(cliente));

        Cliente clienteAtualizado = service.atualizar(1L, clienteRequestDTO);

        Assertions.assertThat(clienteAtualizado).isNotNull();
        Assertions.assertThat(clienteAtualizado.getId()).isEqualTo(cliente.getId());
        Assertions.assertThat(clienteAtualizado.getCpf()).isEqualTo(cliente.getCpf());
        Assertions.assertThat(clienteAtualizado.getNome()).isEqualTo(cliente.getNome());
        Assertions.assertThat(clienteAtualizado.getNascimento()).isEqualTo(cliente.getNascimento());





    }

    @Test
    @DisplayName("Deve lançar exceção ao tentar atualizar cliente inexistente")
    public void atualizarClienteInexistente(){
        ClienteRequestDTO clienteRequestDTO = getNewClienteDTO();
        Cliente cliente = getNewCliente();
        cliente.setId(1L);

        Mockito.when( repository.findById(Mockito.anyLong())).thenReturn(Optional.empty());

        Throwable exception = Assertions.catchThrowable( () -> service.atualizar(1L, clienteRequestDTO));

        Assertions.assertThat(exception).isInstanceOf(ClienteNotFoundException.class);

        Mockito.verify( repository, Mockito.never()).save(cliente);

    }

    @Test
    @DisplayName("Deve lançar exceção por cliente com CPF duplicado")
    public void atualizarClienteCPFDuplicado(){
        ClienteRequestDTO clienteRequestDTO = getNewClienteDTO();
        Cliente cliente = getNewCliente();
        cliente.setId(1L);

        Mockito.when( repository.findById(Mockito.anyLong())).thenReturn(Optional.of(cliente));

        Mockito.when( repository.existsByCpf(Mockito.anyString())).thenReturn(true);

        Throwable exception = Assertions.catchThrowable( () -> service.atualizar(1L, clienteRequestDTO));

        Assertions.assertThat(exception).isInstanceOf(ClienteCPFDuplicadoException.class);

        Mockito.verify( repository, Mockito.never()).save(cliente);
    }

    @Test
    @DisplayName("Excluir cliente")
    public void excluirCliente(){
        Cliente cliente = getNewCliente();
        cliente.setId(1L);

        Mockito.when( repository.findById(Mockito.anyLong())).thenReturn(Optional.of(cliente));

        service.excluir(1L);

        Mockito.verify( repository, Mockito.times(1)).delete(cliente);
    }

    @Test
    @DisplayName("Deve lançar exceção ao tentar excluir cliente inexistente")
    public void excluirClienteInexistente(){

        Cliente cliente = getNewCliente();
        cliente.setId(1L);

        Mockito.when( repository.findById(Mockito.anyLong())).thenReturn( Optional.empty() );

        Throwable exception = Assertions.catchThrowable( () -> service.excluir(1L));

        Assertions.assertThat(exception).isInstanceOf(ClienteNotFoundException.class);

        Mockito.verify( repository, Mockito.never()).delete(cliente);
    }

    public static Cliente getNewCliente() {

        return Cliente.builder()
                .cpf("52477178059")
                .nome("Fulano de Assis")
                .nascimento(LocalDate.of(1990, 1, 1))
                .build();
    }

    public static ClienteRequestDTO getNewClienteDTO(){
        return ClienteRequestDTO.builder()
                .cpf("52477178059")
                .nome("Fulano de Assis")
                .nascimento(LocalDate.of(1990, 1, 1))
                .build();
    }
}
