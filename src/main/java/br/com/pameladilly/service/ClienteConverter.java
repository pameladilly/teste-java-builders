package br.com.pameladilly.service;

import br.com.pameladilly.domain.entity.Cliente;
import br.com.pameladilly.rest.dto.ClienteRequestDTO;
import br.com.pameladilly.rest.dto.ClienteResponseDTO;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.Period;

@Component
public final class ClienteConverter {


    public static Cliente converterDto(ClienteRequestDTO clienteRequestDTO) {

        return Cliente.builder()
                .cpf(clienteRequestDTO.getCpf())
                .nome(clienteRequestDTO.getNome())
                .nascimento(clienteRequestDTO.getNascimento())
                .build();
    }

    public static ClienteResponseDTO converterDto(Cliente cliente){

        Period idade = Period.between( cliente.getNascimento(), LocalDate.now());

        return ClienteResponseDTO.builder()
                .id(cliente.getId())
                .cpf(cliente.getCpf())
                .nome(cliente.getNome())
                .nascimento(cliente.getNascimento())
                .idade(Long.valueOf(idade.getYears()))
                .build();
    }

}
