package br.com.pameladilly.service.impl;

import br.com.pameladilly.domain.entity.Cliente;
import br.com.pameladilly.domain.repository.ClientesRepository;
import br.com.pameladilly.exception.cliente.ClienteCPFDuplicadoException;
import br.com.pameladilly.exception.cliente.ClienteNotFoundException;
import br.com.pameladilly.rest.dto.ClienteRequestDTO;
import br.com.pameladilly.service.ClienteConverter;
import br.com.pameladilly.service.ClienteService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
@RequiredArgsConstructor
public class ClienteServiceImpl implements ClienteService {

    private final ClientesRepository repository;


    @Override
    public Cliente salvar(ClienteRequestDTO clienteRequestDTO) {


        Cliente cliente = ClienteConverter.converterDto(clienteRequestDTO);

        if (repository.existsByCpf(cliente.getCpf())) {
            throw new ClienteCPFDuplicadoException();
        }

        return repository.save( cliente );

    }

    @Override
    public Cliente atualizar(Long id, ClienteRequestDTO clienteRequestDTO) {

        return  repository.findById(id).map(
                cliente -> {
                    cliente.setCpf(clienteRequestDTO.getCpf());
                    cliente.setNascimento(clienteRequestDTO.getNascimento());
                    cliente.setNome(clienteRequestDTO.getNome());

                    if(repository.existsByCpf(clienteRequestDTO.getCpf())) {
                        throw new ClienteCPFDuplicadoException();
                    }

                    return repository.save(cliente);
                }
        ).orElseThrow(ClienteNotFoundException::new);

    }

    @Override
    public Cliente atualizarNascimento(Long id, LocalDate nascimento) {
        return repository.findById(id).map(
                cliente -> {
                    cliente.setNascimento(nascimento);
                    return repository.save(cliente);
                }
        ).orElseThrow(ClienteNotFoundException::new);
    }

    @Override
    public void excluir(Long id) {

        Cliente cliente = repository.findById(id).orElseThrow(ClienteNotFoundException::new);

        repository.delete(cliente);
    }



    @Override
    public Page<Cliente> pesquisar(String nome, String cpf, Pageable pageRequest) {

       return repository.findByNomeOrCpf(nome, cpf, pageRequest);

    }




}
