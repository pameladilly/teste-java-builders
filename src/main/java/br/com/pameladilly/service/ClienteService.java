package br.com.pameladilly.service;

import br.com.pameladilly.domain.entity.Cliente;
import br.com.pameladilly.rest.dto.ClienteRequestDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.time.LocalDate;

public interface ClienteService {

    Cliente salvar(ClienteRequestDTO clienteRequestDTO);

    Cliente atualizar(Long id, ClienteRequestDTO clienteRequestDTO);

    Cliente atualizarNascimento(Long id, LocalDate nascimento);

    void excluir(Long id);

    Page<Cliente> pesquisar(String nome, String cpf, Pageable pageRequest);


}
