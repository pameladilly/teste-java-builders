package br.com.pameladilly.exception.cliente;


public class ClienteCPFDuplicadoException extends RuntimeException {

    public static final String MSG = "Já existe um cliente cadastrado com o CPF informado.";

    public ClienteCPFDuplicadoException() {
        super(MSG);
    }
}
