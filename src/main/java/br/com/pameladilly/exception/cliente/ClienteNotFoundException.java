package br.com.pameladilly.exception.cliente;

public class ClienteNotFoundException extends RuntimeException {

    public static final String MSG = "Cliente não encontrado na base de dados";

    public ClienteNotFoundException() {
        super(MSG);
    }
}
