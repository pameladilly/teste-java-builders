package br.com.pameladilly.rest.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
@Component
public class ClienteResponseDTO {

    private Long id;
    private String nome;
    private String cpf;
    private LocalDate nascimento;
    private Long idade;
}
