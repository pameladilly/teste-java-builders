package br.com.pameladilly.rest.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;
import java.time.LocalDate;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Component
public class ClienteRequestDTO {

    @NotNull(message = "Informe o nome")
    private String nome;

    @NotNull(message = "Informe o CPF")
    @Size(min = 11, max = 11, message = "CPF inválido")
    private String cpf;

    @Past(message = "Informe a data de nascimento")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy" )
    private LocalDate nascimento;
}
