package br.com.pameladilly.rest.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import javax.validation.constraints.Past;
import java.time.LocalDate;

@Component
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ClienteNascimentoRequestDTO {

    @Past(message = "Informe a data de nascimento")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy" )
    private LocalDate nascimento;

}
