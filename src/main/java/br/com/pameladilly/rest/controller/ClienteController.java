package br.com.pameladilly.rest.controller;

import br.com.pameladilly.domain.entity.Cliente;
import br.com.pameladilly.rest.dto.ClienteNascimentoRequestDTO;
import br.com.pameladilly.rest.dto.ClienteRequestDTO;
import br.com.pameladilly.rest.dto.ClienteResponseDTO;
import br.com.pameladilly.service.ClienteConverter;
import br.com.pameladilly.service.ClienteService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/api/cliente")
@RequiredArgsConstructor
public class ClienteController {

    private final ClienteService service;

    @PostMapping
    public ResponseEntity<ClienteResponseDTO> salvar(@RequestBody @Valid ClienteRequestDTO clienteRequestDTO){

        return new ResponseEntity<>(ClienteConverter.converterDto(service.salvar(clienteRequestDTO))   , HttpStatus.CREATED);

    }

    @GetMapping
    public Page<ClienteResponseDTO> pesquisar( String nome,  String cpf, Pageable pageable){

        Page<Cliente> result = service.pesquisar(nome, cpf, pageable);
        List<ClienteResponseDTO> clientes = result.getContent().stream().map(
                ClienteConverter::converterDto
        ).collect(Collectors.toList());

        return new PageImpl<>(clientes, pageable, result.getTotalElements());
    }

    @DeleteMapping("{id}")
    @ResponseStatus(HttpStatus.OK)
    public void excluir(@PathVariable Long id) {

        service.excluir(id);


    }

    @PutMapping("{id}")
    public ResponseEntity<ClienteResponseDTO> atualizar(@PathVariable Long id, @RequestBody @Valid ClienteRequestDTO clienteRequestDTO){

        return new ResponseEntity<>( ClienteConverter.converterDto(service.atualizar(id, clienteRequestDTO)) , HttpStatus.OK);
    }

    @PatchMapping("{id}")
    public ResponseEntity<ClienteResponseDTO> atualizarNascimento(@PathVariable Long id, @RequestBody @Valid ClienteNascimentoRequestDTO clienteNascimentoRequestDTO){

         return new ResponseEntity<>( ClienteConverter.converterDto(service.atualizarNascimento(id, clienteNascimentoRequestDTO.getNascimento())) , HttpStatus.OK);

    }


}
