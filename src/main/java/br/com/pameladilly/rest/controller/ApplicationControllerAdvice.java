package br.com.pameladilly.rest.controller;

import br.com.pameladilly.exception.cliente.ClienteCPFDuplicadoException;
import br.com.pameladilly.exception.cliente.ClienteNotFoundException;
import br.com.pameladilly.rest.ApiErrors;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.support.DefaultHandlerExceptionResolver;

import java.time.format.DateTimeParseException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@RestControllerAdvice
public class ApplicationControllerAdvice {

    @ExceptionHandler( value = {MethodArgumentNotValidException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ApiErrors handleMethodNotValidException(MethodArgumentNotValidException ex) {
        List<String> errors = ex.getBindingResult()
                .getAllErrors()
                .stream()
                .map(DefaultMessageSourceResolvable::getDefaultMessage)
                .collect(Collectors.toList());

        return new ApiErrors(errors);
    }

    @ExceptionHandler(ClienteNotFoundException.class)
    @ResponseStatus( HttpStatus.NOT_FOUND)
    public ApiErrors handleClienteNotFoundException(ClienteNotFoundException ex){
        return new ApiErrors(ex.getMessage());
    }

    @ExceptionHandler(ClienteCPFDuplicadoException.class)
    @ResponseStatus( HttpStatus.BAD_REQUEST)
    public ApiErrors handleClienteCPFDuplicadoException(ClienteCPFDuplicadoException ex){
        return new ApiErrors(ex.getMessage());
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ApiErrors handleClienteNotFoundException(HttpMessageNotReadableException ex){
        List<String> erros = Arrays.asList("Não foi possível ler o formato de dados.", ex.getMessage());
        return new ApiErrors(erros);

    }



}
