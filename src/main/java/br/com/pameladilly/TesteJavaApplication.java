package br.com.pameladilly;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
public class TesteJavaApplication {

	public static void main(String[] args) {
		SpringApplication.run(TesteJavaApplication.class, args);
	}

}
