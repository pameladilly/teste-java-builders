package br.com.pameladilly.domain.repository;

import br.com.pameladilly.domain.entity.Cliente;
import ch.qos.logback.core.net.server.Client;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QPageRequest;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ClientesRepository extends JpaRepository<Cliente, Long> {


    @Query( value = "select c from Cliente c where c.nome like %:nome% or c.cpf = :cpf")
    Page<Cliente> findByNomeOrCpf(
            @Param("nome") String nome,
            @Param("cpf" ) String cpf,
            Pageable pageable);

    boolean existsByCpf(String cpf);
}
